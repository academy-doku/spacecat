package model;

public class Missile {

    private int x;
    private int y;
    private float speedY;


    public Missile(int x, int y, float speedY) {
        this.x = x; //hängt vom Player ab
        this.y = y; //automatisch
        this.speedY = 0.2f;
    }


    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public float getSpeedY() {
        return speedY;
    }

    public void update(long timePassed) {
        this.y = Math.round(this.y + timePassed * this.speedY * -1);
    }

    public void moveTo(int dx, int dy) {
        this.x += dx;
        this.y -= dy;
    }

    public int getHeight() {
        return 10;
    }

    public int getWidth() {
        return 5;
    }


}
